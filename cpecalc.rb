#!/usr/bin/env ruby

# Provides information about a SCORM course to help determine the number of CPEs given

$: << 'lib'
require 'tmpdir'
require 'json'
require 'zip'
require 'slop' # Options library
require 'mp3info'
require 'core_extensions/fixnum/empty'

# Monkey-patch Fixnum
class Fixnum
  include CoreExtensions::Fixnum::Empty
end

@course_dir = ''
@mp3_count = 0
@total_time = 0
@kc_count = 0
@slide_count = 0

def unzip_scorm(zipfile, outputdir)
  extract_pattern = ['courseStructure.js', '**/*.mp3']

  Zip.on_exists_proc = true
  Zip::File.open(zipfile) do |file|
    extract_pattern.each do |match|
      file.glob(match).each do |f|
        output_file = outputdir + File::SEPARATOR + f.name
        # Ensure the path exists
        FileUtils::mkdir_p File::dirname(output_file)
        f.extract(output_file)
        printf '.'
        if File.extname(output_file) == '.mp3'
          mp3 = Mp3Info.open(output_file)
          @total_time += mp3.length
          @mp3_count += 1
          mp3.close
        elsif File.extname(output_file) == '.js'
          js = File.read(output_file)
          json = JSON.parse(claro_js_to_json(js))
          json['modules'].each do |m|
            m['objects'].each do |o|
              o['subeos'].each do |subeo|
                @kc_count += 1 if !(subeo['practice_question_page_pageid'] && subeo['practice_question_page_pageid'].empty?)
                @slide_count += 1 if !(subeo['content_page_pageid'] && subeo['content_page_pageid'].empty?)
              end
            end
          end
        end
        File::delete output_file
      end
    end
  #Dir::rmdir outputdir
  puts
  end
end

def claro_js_to_json(js)
  return js.gsub!(/^var courseStructure = /, "")
end

def main
  opts = Slop.parse do |o|
    o.string '-s', '--scormfile', 'Claro SCORM file'
  end

  @course_dir = opts[:outputdir] || Dir.mktmpdir
  unzip_scorm(opts[:scormfile], @course_dir)
  
  hrs = (@total_time / 3600).to_i
  mins = ((@total_time - (hrs * 3600)).to_i / 60).to_i
  secs = (@total_time % 60).to_i

  puts "Total audio length: #{hrs}h #{mins}m #{secs}s"
  puts "Total audio files: #{@mp3_count}"
  puts
  puts "Total slide count: #{@slide_count}"
  puts "Total Knowledge Checks: #{@kc_count}"
end

main
