# Mac Installation

1. Run ```git``` from a terminal command line. If prompted to install developer tools you must do so prior to continuing. 

2. Run ```git clone https://garethbowker@bitbucket.org/garethbowker/cpecalc.git```

3. Change into the cpecalc directory ```cd cpecalc``` from the current working directory as this is where the tool was cloned. 

4. The tool requires the Scorm Package as part of the input to run. Example below ```./cpecalc.rb -s /Users/tpowell/Downloads/4E359C49-DBC5-3F10-ACB2-37FFD09F5BA5.zip```

5. You will most likely get some library errors if you've not used Ruby (the programming language used) for other tasks. Basic errors can be resolved by installing the required libraries, which you can do by running:
```bash
        sudo gem install rubyzip
        sudo gem install slop
        sudo gem install mp3info
```

If any other libraries are missing from the above installation install them as they will be returned in the ruby error presented on screen. 
	
6. Run the script with the required inputs. ```./cpecalc.rb -s <ScormPackage.zip>``` This will return the audio results to calculate CPE hours for a given course as shown below. 

```
TPowell-Macbook-Pro:cpecalc tpowell$ ./cpecalc.rb -s /Users/tpowell/Downloads/4E359C49-DBC5-3F10-ACB2-37FFD09F5BA5.zip
.......................................................................................................................................................................................................................
Total audio length: 3h 21m 55s
Total audio files: 214

Total slide count: 234
Total Knowledge Checks: 22
```