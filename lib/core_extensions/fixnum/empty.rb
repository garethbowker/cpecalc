# Monkey-patch Fixnum to have a .empty? method so we don't have to worry about type later
module CoreExtensions
  module Fixnum
    module Empty
      def empty?
        return self == 0
      end
    end
  end
end

